import java.sql.SQLOutput;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DrawSquare {
    public static void main(String[] args) {
        drawMethod();
    }

    public static void drawMethod(){
        //Initializing variables
        Scanner input = new Scanner(System.in);
        StringBuilder sb = new StringBuilder();

        int firstInput = inputTyper(input);
        int secondInput = inputTyper(input);

        //For loops for creating a symbol matrix
        for (int i = 0; i<firstInput;i++) {
            for (int j= 0;j<secondInput;j++) {
                //Check if input is under 0
                if(firstInput<0 || secondInput<0){
                    System.out.println("Invalid input");
                    break;
                }

                //Checking that we're at the edge (using multiple else if's for readability)
                if((i==0 || i==firstInput-1) || (j==0 ||j==secondInput-1)){
                    sb.append("#");
                }
                //Checking if we're on the third/third-to-last row, but not on the second/second-to-last column
                else if ((i==2||i==firstInput-3) && (j!=1 && j!= secondInput-2) && (firstInput>4)){
                    sb.append("#");
                }
                //Checking if we're on the third/third-to-last column, but not on the second/second-to-last row
                else if ((j==2||j==secondInput-3) && (i!=1 && i!= firstInput-2) && (secondInput>4)){
                    sb.append("#");
                }
                //Adding blank space if previous conditions were not satisfied
                else {
                    sb.append(" ");
                }
            }
            //Starting new line
            sb.append("\n");
        }

        System.out.println(sb.toString());
    }

    public static int inputTyper(Scanner input){
        //Checking input strings
        int output=-1;
        boolean reading = true;
        while(reading) {
            System.out.println("Please type a valid integer");
            String inputStr = input.next();
            try {
                output = getValidInput(inputStr);
                reading = false;
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        return output;
    }

    public static int getValidInput(String input) throws Exception {
        Pattern isDigits = Pattern.compile("[0-9]*");
        Matcher lineChecker = isDigits.matcher(input);
        if(!lineChecker.matches()) {
            throw new InputMismatchException("Invalid input. Please type a valid integer");
        }
        return Integer.valueOf(input);
    }
}
